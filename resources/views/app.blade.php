<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>

        <title>Dodgy Bros</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        @vite(['resources/css/app.css', 'resources/js/app.js'])

        <script>
            const ADMIN = @json($aAdminSessions ?? []);
        </script>
    </head>
    <body>
        <div id="app"></div>
    </body>
</html>
