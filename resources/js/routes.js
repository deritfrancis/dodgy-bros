import {createRouter, createWebHistory} from 'vue-router';
import LoginForm from './Components/LoginForm.vue';
import CarsList from './Components/Cars/CarsList.vue';
import CarsListForm from './Components/Cars/CarsListForm.vue';
import CarsBuyForm from './Components/Cars/CarsBuyForm.vue';

import SuccessPage from './Components/SuccessPage.vue';

export const oRouter = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: CarsList
        },
        {
            path: '/login',
            component: LoginForm
        },
        {
            path: '/form',
            component: CarsListForm
        },
        {
            path: '/buy',
            component: CarsBuyForm
        },
        {
            path: '/success',
            component: SuccessPage
        },
    ]
})
