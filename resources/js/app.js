import './bootstrap';
import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { oRouter } from './routes.js';

import App from './App.vue';

const oPinia = createPinia();

createApp(App)
    .use(oRouter)
    .use(oPinia)
    .mount('#app');
