export const oCarRequests = {
    /**
     * Request for get cars list
     *
     * @param oParams
     * @returns {Promise<axios.AxiosResponse<any>>}
     */
    getCars(oParams) {
        return axios.get('/api/cars', { params: oParams });
    },

    /**
     * Request for add car to list
     *
     * @param oParams
     * @returns {Promise<axios.AxiosResponse<any>>}
     */
    addCar(oParams) {
        return axios.post('/api/cars', oParams);
    },

    /**
     * Request for edit car from list (buy, availability)
     *
     * @param iCarNo
     * @param oParams
     * @returns {Promise<axios.AxiosResponse<any>>}
     */
    editCar(iCarNo, oParams) {
        return axios.put('/api/cars/' + iCarNo, oParams);
    }
}

export const oAuthRequests = {
    /**
     * Request for authenticating admin user
     *
     * @param oParams
     * @returns {Promise<axios.AxiosResponse<any>>}
     */
    authAdmin(oParams) {
        return axios.post('/auth', oParams);
    }
}
