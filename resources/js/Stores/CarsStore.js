import { defineStore } from 'pinia';
import { oCarRequests } from "@/requests";

export const useCarsStore = defineStore('cars', {
    state: () => ({
        iCarsCount: 0,

        aCarsList: [],
        oCarParams: {
            car_name: null,
            car_model: null,
            condition: null,
            year: null,
            price: null,
            seller_name: null,
            seller_number: null
        },

        oBuyCarParams: {
            buyer_name: null,
            buyer_number: null
        },

        oGetCarsParams: {
            offset: 0,
            category: null,
            value: null
        },

        sSearchValue: undefined,
        sSearchCategory: undefined,
    }),
    actions: {
        /**
         * This will fetch all the listed cars
         */
        getCars() {
            let oThis = this;

            oCarRequests.getCars(this.oGetCarsParams)
                .then((oResponse) => {
                    oThis.aCarsList = oResponse.data.cars;
                    oThis.iCarsCount = oResponse.data.count;
                });
        },

        /**
         * This will process the selling of a car
         */
        listCar() {
            let bValid = this.validateRequiredParams(this.oCarParams);

            if (bValid === true) {
                oCarRequests.addCar(this.oCarParams)
                    .then((oResponse) => {
                        let bListed = Boolean(oResponse.data)

                        if (bListed === true) {
                            return window.location.href = '/success';
                        }
                    })
                    .catch(() => {
                        alert('Error listing a car');
                    });
            } else {
                alert('Please fill up the form');
            }
        },

        /**
         * This will process the buying of a car
         */
        buyCar() {
            let bValid = this.validateRequiredParams(this.oBuyCarParams);

            if (bValid === true) {
                let iCarNo = new URLSearchParams(window.location.search).get('car_no');

                oCarRequests.editCar(iCarNo, this.oBuyCarParams)
                    .then((oResponse) => {
                        let bBought = Boolean(oResponse.data)

                        if (bBought === true) {
                            return window.location.href = '/success';
                        }
                    })
                    .catch(() => {
                        alert('Error buying a car');
                    });
            } else {
                alert('Please fill up the form');
            }
        },

        /**
         * This will process the updating of a car to available
         */
        availableCar(iCarNo) {
            oCarRequests.editCar(iCarNo, this.oBuyCarParams)
                .then((oResponse) => {
                    let bBought = Boolean(oResponse.data)

                    if (bBought === true) {
                        return window.location.href = '/';
                    }
                })
                .catch(() => {
                    alert('Error updating a car to available');
                });
        },

        /**
         * This will validate required parameters for car-related forms
         *
         * @param oParams
         * @returns {boolean}
         */
        validateRequiredParams(oParams) {
            for (const sKey in oParams) {
                if (oParams[sKey] === null) {
                    document.getElementById(sKey).focus();

                    return false;
                }
            }

            return true;
        },

    }
});
