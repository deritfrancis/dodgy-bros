import { defineStore } from 'pinia';

export const useAdminStore = defineStore('admin', {
    state: () => ({
        oAdmin: {}
    }),
    getters: {
        /**
         * Getters for admin session name
         *
         * @param oState
         * @returns {*}
         */
        sAdminName(oState) {
            return oState.oAdmin.admin.admin_name
        },

        /**
         * Getters for admin if it has session
         *
         * @param oState
         * @returns {*}
         */
        bHasSession(oState) {
            return oState.oAdmin.has_session;
        }
    },
    actions: {
        /**
         * This will set the admin details
         *
         * @param oAdmin
         */
        setAdmin(oAdmin) {
            this.oAdmin = oAdmin;
        }
    }
});
