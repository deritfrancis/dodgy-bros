import { defineStore } from 'pinia';

import { oAuthRequests } from '@/requests';

export const useAuthStore = defineStore('auth', {
    state: () => ({
        sUsername: undefined,
        sPassword: undefined
    }),
    actions: {
        /**
         * This will process the authentication of admin
         */
        authAdmin() {
            let oThis = this;

            let oParams = {
                admin_username: oThis.sUsername,
                admin_password: oThis.sPassword
            }

            oAuthRequests.authAdmin(oParams)
                .then((oResponse) => {
                    let bAuthenticate = Boolean(oResponse.data);

                    if (bAuthenticate === true) {
                        return window.location.href = '/';
                    }
                })
                .catch(() => {
                    alert('Incorrect username or password');
                });
        }
    }
});
