<?php

use App\Constants\CarsConstants;
use App\Constants\SellersConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(CarsConstants::TABLE_CARS, function (Blueprint $table) {
            $table->id(CarsConstants::COLUMN_CAR_NO);
            $table->bigInteger(CarsConstants::COLUMN_SELLER_NO)->unsigned();
            $table->string(CarsConstants::COLUMN_CAR_NAME);
            $table->string(CarsConstants::COLUMN_CAR_MODEL);
            $table->smallInteger(CarsConstants::COLUMN_YEAR)->unsigned();
            $table->enum(CarsConstants::COLUMN_CONDITION, CarsConstants::COLUMN_CONDITION_ENUM_VALUES);
            $table->float(CarsConstants::COLUMN_PRICE);
            $table->tinyInteger(CarsConstants::COLUMN_IS_AVAILABLE)->default(1);

            $table->foreign(CarsConstants::COLUMN_SELLER_NO)
                ->references(SellersConstants::COLUMN_SELLER_NO)
                ->on(SellersConstants::TABLE_SELLERS);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(CarsConstants::TABLE_CARS);
    }
};
