<?php

use App\Constants\SellersConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(SellersConstants::TABLE_SELLERS, function (Blueprint $table) {
            $table->id(SellersConstants::COLUMN_SELLER_NO);
            $table->string(SellersConstants::COLUMN_SELLER_NAME);
            $table->string(SellersConstants::COLUMN_SELLER_NUMBER, 15);
        });

        DB::table(SellersConstants::TABLE_SELLERS)->insert([
            SellersConstants::COLUMN_SELLER_NAME   => 'Francis Derit',
            SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(SellersConstants::TABLE_SELLERS);
    }
};
