<?php

use App\Constants\AdminConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(AdminConstants::TABLE_ADMIN, function (Blueprint $table) {
            $table->id(AdminConstants::COLUMN_ADMIN_NO);
            $table->string(AdminConstants::COLUMN_ADMIN_NAME);
            $table->string(AdminConstants::COLUMN_ADMIN_USERNAME);
            $table->string(AdminConstants::COLUMN_ADMIN_PASSWORD);
        });

        DB::table('t_admin')->insert([
            AdminConstants::COLUMN_ADMIN_NAME     => 'Iron Mike',
            AdminConstants::COLUMN_ADMIN_USERNAME => 'mike@example.org',
            AdminConstants::COLUMN_ADMIN_PASSWORD => 'mikeymike123',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(AdminConstants::TABLE_ADMIN);
    }
};
