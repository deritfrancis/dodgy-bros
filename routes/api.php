<?php

use App\Http\Controllers\CarsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(CarsController::class)->group(function() {
    Route::get('cars', 'getCars');
    Route::post('cars', 'saveCar');
    Route::put('cars/{iCarNo}', 'editCar');
    Route::delete('cars/{iCarNo}', 'removeCar');
});
