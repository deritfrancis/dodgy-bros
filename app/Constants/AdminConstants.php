<?php

namespace App\Constants;

/**
 * @class AdminConstants
 * @package App\Constants
 * @author Francis Derit
 * @since 04.16.23
 */
class AdminConstants extends BaseConstants
{
    public const ENTITY = 'admin';

    // TABLE NAME
    public const TABLE_ADMIN = 't_admin';

    // COLUMN NAMES FOR ADMIN TABLE
    public const COLUMN_ADMIN_NO = 'admin_no';
    public const COLUMN_ADMIN_NAME = 'admin_name';
    public const COLUMN_ADMIN_USERNAME = 'admin_username';
    public const COLUMN_ADMIN_PASSWORD = 'admin_password';

    // FIELDS THAT CAN BE FETCHED
    public const FETCH_ABLE_FIELDS = [
        self::COLUMN_ADMIN_USERNAME,
        self::COLUMN_ADMIN_PASSWORD
    ];
}
