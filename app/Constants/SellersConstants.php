<?php

namespace App\Constants;

/**
 * @class SellersConstants
 * @package App\Constants
 * @author Francis Derit
 * @since 04.16.23
 */
class SellersConstants extends BaseConstants
{
    // ENTITY NAME
    public const ENTITY = 'sellers';

    // TABLE NAME
    public const TABLE_SELLERS = 't_sellers';

    // COLUMN NAMES FOR SELLERS TABLE
    public const COLUMN_SELLER_NO = 'seller_no';
    public const COLUMN_SELLER_NAME = 'seller_name';
    public const COLUMN_SELLER_NUMBER = 'seller_number';

    // FIELDS THAT CAN BE CREATED
    public const CREATE_ABLE_FIELDS = [
        self::COLUMN_SELLER_NAME,
        self::COLUMN_SELLER_NUMBER,
    ];
}
