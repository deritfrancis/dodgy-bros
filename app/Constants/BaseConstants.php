<?php

namespace App\Constants;

/**
 * @class BaseConstants
 * @package App\Constants
 * @author Francis Derit
 * @since 04.16.23
 */
class BaseConstants
{
    public const DATA = 'data';

    public const OFFSET = 'offset';
    public const LIMIT = 'limit';
    public const DESC = 'desc';
    public const HAS_SESSION = 'has_session';

    public const COUNT = 'count';

    public const PARAM = 'param';
    public const RESULT = 'result';
}
