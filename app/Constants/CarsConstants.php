<?php

namespace App\Constants;

/**
 * @class CarsConstants
 * @package App\Constants
 * @author Francis Derit
 * @since 04.16.23
 */
class CarsConstants extends BaseConstants
{
    // ENTITY NAME
    public const ENTITY = 'cars';

    // TABLE NAME
    public const TABLE_CARS = 't_cars';

    // COLUMN NAME FOR CARS TABLE
    public const COLUMN_CAR_NO = 'car_no';
    public const COLUMN_SELLER_NO = 'seller_no';
    public const COLUMN_CAR_NAME = 'car_name';
    public const COLUMN_CAR_MODEL = 'car_model';
    public const COLUMN_YEAR = 'year';
    public const COLUMN_CONDITION = 'condition';
    public const COLUMN_PRICE = 'price';
    public const COLUMN_IS_AVAILABLE = 'is_available';

    // BUYER PARAMS
    public const BUYER_NAME = 'buyer_name';
    public const BUYER_NUMBER = 'buyer_number';

    // FILTER PARAMS
    public const CATEGORY = 'category';
    public const VALUE = 'value';

    // COLUMN CONDITION ENUM VALUES
    public const POOR = 'poor';
    public const FAIR = 'fair';
    public const GOOD = 'good';
    public const EXCELLENT = 'excellent';

    public const COLUMN_CONDITION_ENUM_VALUES = [
        self::POOR,
        self::FAIR,
        self::GOOD,
        self::EXCELLENT,
    ];

    // FIELDS THAT CAN BE CREATED
    public const CREATE_ABLE_FIELDS = [
        self::COLUMN_CAR_NAME,
        self::COLUMN_CAR_MODEL,
        self::COLUMN_YEAR,
        self::COLUMN_CONDITION,
        self::COLUMN_PRICE,
    ];

    // FIELDS THAT CAN BE FETCHED
    public const FETCH_ABLE_FIELD = [
        self::CATEGORY,
        self::VALUE,
    ];
}
