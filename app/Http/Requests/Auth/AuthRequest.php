<?php

namespace App\Http\Requests\Auth;

use App\Constants\AdminConstants;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @class AuthRequest
 * @package App\Http\Requests\Auth
 * @author Francis Derit
 * @since 04.16.23
 */
class AuthRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            AdminConstants::COLUMN_ADMIN_USERNAME => 'required|email',
            AdminConstants::COLUMN_ADMIN_PASSWORD => 'required'
        ];
    }
}
