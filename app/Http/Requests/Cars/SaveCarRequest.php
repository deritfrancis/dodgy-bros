<?php

namespace App\Http\Requests\Cars;

use App\Constants\CarsConstants;
use App\Constants\SellersConstants;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @class SaveCarRequest
 * @package App\Http\Requests\Cars
 * @author Francis Derit
 * @since 04.16.23
 */
class SaveCarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            CarsConstants::COLUMN_CAR_NAME         => 'required|string',
            CarsConstants::COLUMN_CAR_MODEL        => 'required|string',
            CarsConstants::COLUMN_YEAR             => 'required|digits:4|integer|min:1900|max:' . date('Y'),
            CarsConstants::COLUMN_CONDITION        => 'required|in:poor,fair,good,excellent',
            CarsConstants::COLUMN_PRICE            => 'required|integer',
            SellersConstants::COLUMN_SELLER_NAME   => 'required|string',
            SellersConstants::COLUMN_SELLER_NUMBER => 'required|string'
        ];
    }
}
