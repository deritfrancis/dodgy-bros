<?php

namespace App\Http\Requests\Cars;

use App\Constants\CarsConstants;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @class EditCarRequest
 * @package App\Http\Requests\Cars
 * @author Francis Derit
 * @since 04.16.23
 */
class EditCarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            CarsConstants::BUYER_NAME => 'string|nullable',
            CarsConstants::BUYER_NUMBER => 'string|nullable',
        ];
    }
}
