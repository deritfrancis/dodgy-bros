<?php

namespace App\Http\Requests\Cars;

use App\Constants\BaseConstants;
use App\Constants\CarsConstants;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @class GetCarsRequest
 * @package App\Http\Requests\Cars
 * @author Francis Derit
 * @since 04.16.23
 */
class GetCarsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            CarsConstants::CATEGORY => 'string|nullable',
            CarsConstants::VALUE    => 'string|nullable',
            BaseConstants::OFFSET   => 'int'
        ];
    }
}
