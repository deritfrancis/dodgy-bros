<?php

namespace App\Http\Controllers;

use App\Constants\BaseConstants;
use App\Constants\CarsConstants;
use Illuminate\Support\Arr;

/**
 * @class BaseController
 * @package App\Http\Controllers
 * @author Francis Derit
 * @since 04.18.23
 */
class BaseController extends Controller
{
    /**
     * This will format the search params
     *
     * @param array $aParams
     * @param array $aFields
     * @return array
     */
    protected function formatSearchParams(array $aParams, array $aFields): array
    {
        return [
            BaseConstants::DATA   => Arr::only($aParams, $aFields),
            BaseConstants::OFFSET => (int) Arr::get($aParams, BaseConstants::OFFSET, 0),
            BaseConstants::LIMIT  => (int) Arr::get($aParams, BaseConstants::LIMIT, 10)
        ];
    }

    /**
     * This will format the filter params
     *
     * @param array $aParams
     * @return array
     */
    protected function formatFilterParams(array $aParams): array
    {
        $aData = $aParams[BaseConstants::DATA];

        if (Arr::has($aData, CarsConstants::CATEGORY) === true && Arr::has($aData, CarsConstants::VALUE) === true) {
            return [$aData[CarsConstants::CATEGORY] => $aData[CarsConstants::VALUE]];
        }

        return [];
    }
}
