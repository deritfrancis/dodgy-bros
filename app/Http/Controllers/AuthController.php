<?php

namespace App\Http\Controllers;

use App\Constants\AdminConstants;
use App\Http\Requests\Auth\AuthRequest;
use App\Services\AuthService;

/**
 * @class AuthController
 * @package App\Http\Controllers
 * @author Francis Derit
 * @since 04.18.23
 */
class AuthController extends BaseController
{
    /**
     * @var AuthService
     */
    private AuthService $oAuthService;

    public function __construct(AuthService $oAuthService)
    {
        $this->oAuthService = $oAuthService;
    }

    /**
     * Request for authenticating an admin
     *
     * @param AuthRequest $oRequest
     * @return bool
     */
    public function authenticateAdmin(AuthRequest $oRequest): bool
    {
        $aParams = $this->formatSearchParams($oRequest->all(), AdminConstants::FETCH_ABLE_FIELDS);

        return $this->oAuthService->authenticateAdmin($aParams);
    }
}
