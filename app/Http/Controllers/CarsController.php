<?php

namespace App\Http\Controllers;

use App\Constants\BaseConstants;
use App\Constants\CarsConstants;
use App\Http\Requests\Cars\EditCarRequest;
use App\Http\Requests\Cars\GetCarsRequest;
use App\Http\Requests\Cars\SaveCarRequest;
use App\Services\CarsService;

/**
 * @class CarsController
 * @package App\Http\Controllers
 * @author Francis Derit
 * @since 04.18.23
 */
class CarsController extends BaseController
{
    /**
     * @var CarsService
     */
    private CarsService $oCarsService;

    public function __construct(CarsService $oCarsService)
    {
        $this->oCarsService = $oCarsService;
    }

    /**
     * Request for getting cars list
     *
     * @param GetCarsRequest $oRequest
     * @return array
     */
    public function getCars(GetCarsRequest $oRequest): array
    {
        $aParams = $this->formatSearchParams($oRequest->all(), CarsConstants::FETCH_ABLE_FIELD);
        $aParams[BaseConstants::DATA] = $this->formatFilterParams($aParams);

        return $this->oCarsService->getCars($aParams);
    }

    /**
     * Request for saving car to list
     *
     * @param SaveCarRequest $oRequest
     * @return mixed
     */
    public function saveCar(SaveCarRequest $oRequest): mixed
    {
        $aParams = $oRequest->all();

        return $this->oCarsService->saveCar($aParams);
    }

    /**
     * Request for updating car to available and not
     *
     * @param EditCarRequest $oRequest
     * @param int $iCarNo
     * @return false|mixed
     */
    public function editCar(EditCarRequest $oRequest, int $iCarNo): mixed
    {
        $aParams = $oRequest->all();

        return $this->oCarsService->editCar($iCarNo, $aParams);
    }
}
