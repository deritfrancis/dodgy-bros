<?php

namespace App\Http\Controllers;

use App\Services\AdminService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

/**
 * @class AdminController
 * @package App\Http\Controllers
 * @author Francis Derit
 * @since 04.18.23
 */
class AdminController extends Controller
{
    /**
     * @var AdminService
     */
    private AdminService $oAdminService;

    public function __construct(AdminService $oAdminService)
    {
        $this->oAdminService = $oAdminService;
    }

    /**
     * This will initialize the session and return to home page
     *
     * @return Application|Factory|View
     */
    public function getHome(): View|Factory|Application
    {
        $aAdminSessions = $this->oAdminService->getAdminSessions();

        return view('app', ['aAdminSessions' => $aAdminSessions]);
    }

    /**
     * This will log out the admin and destroy session
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function logoutAdmin(): Redirector|RedirectResponse|Application
    {
        $this->oAdminService->destroyAdminSessions();

        return redirect('/');
    }
}
