<?php

namespace App\Repositories;

use App\Models\Sellers;

/**
 * @class SellersRepository
 * @package App\Repositories
 * @author Francis Derit
 * @since 04.18.23
 */
class SellersRepository
{
    /**
     * Create a seller
     *
     * @param array $aParams
     * @return mixed
     */
    public function createSeller(array $aParams): mixed
    {
        return Sellers::firstOrCreate($aParams)->toArray();
    }
}
