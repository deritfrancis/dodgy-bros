<?php

namespace App\Repositories;

use App\Constants\BaseConstants;
use App\Constants\CarsConstants;
use App\Constants\SellersConstants;
use App\Models\Cars;
use Illuminate\Support\Arr;

/**
 * @class CarsRepository
 * @package App\Repositories
 * @author Francis Derit
 * @since 04.16.2023
 */
class CarsRepository
{
    /**
     * Fetch all cars
     *
     * @param array $aParams
     * @return array
     */
    public function fetchCars(array $aParams): array
    {
        $aCars = Cars::with([SellersConstants::ENTITY])
            ->where(Arr::get($aParams, BaseConstants::DATA))
            ->orderBy(CarsConstants::COLUMN_CAR_NO, BaseConstants::DESC);

        $iCarsCount = $aCars->count();

        $aCarsList = $aCars->skip(Arr::get($aParams, BaseConstants::OFFSET))
            ->take(Arr::get($aParams, BaseConstants::LIMIT))
            ->get()
            ->toArray();

        return [$aCarsList, $iCarsCount];
    }

    /**
     * Create a car
     *
     * @param array $aParams
     * @return mixed
     */
    public function createCar(array $aParams): mixed
    {
        return Cars::insert($aParams);
    }

    /**
     * Update a car by car_no
     *
     * @param int $iCarNo
     * @param array $aParams
     * @return mixed
     */
    public function updateCars(int $iCarNo, array $aParams): mixed
    {
        return Cars::where(CarsConstants::COLUMN_CAR_NO, $iCarNo)
            ->update($aParams);
    }

    /**
     * Find a car by car_no
     *
     * @param int $iCarNo
     * @return mixed
     */
    public function findCar(int $iCarNo): mixed
    {
        return Cars::find($iCarNo)->toArray();
    }

}
