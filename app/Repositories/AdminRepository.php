<?php

namespace App\Repositories;

use App\Constants\BaseConstants;
use App\Models\Admin;

/**
 * @class AdminRepository
 * @package App\Repositories
 * @author Francis Derit
 * @since 04.16.2023
 */
class AdminRepository
{
    /**
     * This will fetch the admin user account
     *
     * @param array $aParams
     * @return array
     */
    public function getAdmin(array $aParams): array
    {
        return Admin::where($aParams[BaseConstants::DATA])
            ->get()
            ->toArray();
    }
}
