<?php

namespace App\Models;

use App\Constants\SellersConstants;
use Illuminate\Database\Eloquent\Model;

/**
 * @class Cars
 * @package App\Models
 * @author Francis Derit
 * @since 04.16.23
 */
class Sellers extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = SellersConstants::TABLE_SELLERS;

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = SellersConstants::COLUMN_SELLER_NO;

    /**
     * Creatable fields
     * @var string[]
     */
    protected $fillable = SellersConstants::CREATE_ABLE_FIELDS;

    /**
     * Timestamps
     * @var bool
     */
    public $timestamps = false;
}
