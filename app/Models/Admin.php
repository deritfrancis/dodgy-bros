<?php

namespace App\Models;

use App\Constants\AdminConstants;
use Illuminate\Database\Eloquent\Model;

/**
 * @class Admin
 * @package App\Models
 * @author Francis Derit
 * @since 04.16.23
 */
class Admin extends Model
{
    /**
     * Table Name
     * @var string
     */
    protected $table = AdminConstants::TABLE_ADMIN;

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = AdminConstants::COLUMN_ADMIN_NO;
}
