<?php

namespace App\Models;

use App\Constants\CarsConstants;
use App\Constants\SellersConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @class Cars
 * @package App\Models
 * @author Francis Derit
 * @since 04.16.23
 */
class Cars extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = CarsConstants::TABLE_CARS;

    /**
     * Primary Key
     * @var string
     */
    protected $primaryKey = CarsConstants::COLUMN_CAR_NO;

    /**
     * Creatable fields
     * @var string[]
     */
    protected $fillable = CarsConstants::CREATE_ABLE_FIELDS;

    /**
     * Timestamps
     * @var bool
     */
    public $timestamps = false;

    /**
     * Relationship with sellers model
     * @return HasOne
     */
    public function sellers(): HasOne
    {
        return $this->hasOne(
            Sellers::class,
            SellersConstants::COLUMN_SELLER_NO,
            CarsConstants::COLUMN_SELLER_NO
        );
    }
}
