<?php

namespace App\Services;

use App\Constants\BaseConstants;
use App\Constants\CarsConstants;
use App\Constants\SellersConstants;
use App\Repositories\CarsRepository;
use App\Repositories\SellersRepository;
use Illuminate\Support\Arr;

/**
 * @class CarsService
 * @package App\Services
 * @author Francis Derit
 * @since 04.18.23
 */
class CarsService
{
    /**
     * @var CarsRepository
     */
    private CarsRepository $oCarsRepository;

    /**
     * @var SellersRepository
     */
    private SellersRepository $oSellersRepository;

    public function __construct(CarsRepository $oCarsRepository, SellersRepository $oSellersRepository)
    {
        $this->oCarsRepository = $oCarsRepository;
        $this->oSellersRepository = $oSellersRepository;
    }

    /**
     * This will process the fetching of cars
     *
     * @param array $aParams
     * @return array
     */
    public function getCars(array $aParams): array
    {
        $aCarsResult = $this->oCarsRepository->fetchCars($aParams);

        return [
            CarsConstants::ENTITY => $aCarsResult[0],
            BaseConstants::COUNT  => $aCarsResult[1]
        ];
    }

    /**
     * This will process the saving of car
     *
     * @param array $aParams
     * @return mixed
     */
    public function saveCar(array $aParams): mixed
    {
        $aSellerParams = Arr::only($aParams, [
            SellersConstants::COLUMN_SELLER_NAME,
            SellersConstants::COLUMN_SELLER_NUMBER
        ]);

        $aSellerResult = $this->oSellersRepository->createSeller($aSellerParams);
        $aParams[CarsConstants::COLUMN_SELLER_NO] = Arr::get($aSellerResult, SellersConstants::COLUMN_SELLER_NO);

        return $this->oCarsRepository->createCar(Arr::except($aParams, [
            SellersConstants::COLUMN_SELLER_NAME,
            SellersConstants::COLUMN_SELLER_NUMBER
        ]));
    }

    /**
     * This will process the updating of car
     *
     * @param int $iCarNo
     * @param array $aParams
     * @return false|mixed
     */
    public function editCar(int $iCarNo, array $aParams): mixed
    {
        $aCarResult = $this->oCarsRepository->findCar($iCarNo);

        if (empty($aCarResult) === true) {
            return false;
        }

        // TODO: Email Iron Mike

        $aUpdateParams = [
            CarsConstants::COLUMN_IS_AVAILABLE => $aParams[CarsConstants::BUYER_NAME] === null && $aParams[CarsConstants::BUYER_NUMBER] === null
        ];

        return $this->oCarsRepository->updateCars($iCarNo, $aUpdateParams);
    }
}
