<?php

namespace App\Services;

use App\Constants\AdminConstants;
use App\Repositories\AdminRepository;

/**
 * @class AuthService
 * @package App\Services
 * @author Francis Derit
 * @since 04.18.23
 */
class AuthService
{
    /**
     * @var AdminRepository
     */
    private AdminRepository $oAdminRepository;

    public function __construct(AdminRepository $oAdminRepository)
    {
        $this->oAdminRepository = $oAdminRepository;
    }

    /**
     * This will authenticate the admin user
     *
     * @param array $aParams
     * @return bool
     */
    public function authenticateAdmin(array $aParams): bool
    {
        $aAdmin = $this->oAdminRepository->getAdmin($aParams);

        if (empty($aAdmin) === true) {
            return false;
        }

        session()->put([
            AdminConstants::COLUMN_ADMIN_NO       => $aAdmin[0][AdminConstants::COLUMN_ADMIN_NO],
            AdminConstants::COLUMN_ADMIN_NAME     => $aAdmin[0][AdminConstants::COLUMN_ADMIN_NAME],
            AdminConstants::COLUMN_ADMIN_USERNAME => $aAdmin[0][AdminConstants::COLUMN_ADMIN_USERNAME],
        ]);

        return true;
    }
}
