<?php

namespace App\Services;

use App\Constants\AdminConstants;
use App\Constants\BaseConstants;
use Illuminate\Support\Arr;

/**
 * @class AdminService
 * @package App\Services
 * @author Francis Derit
 * @since 04.18.23
 */
class AdminService
{
    /**
     * This will get the session and check if session is valid
     *
     * @return array
     */
    public function getAdminSessions(): array
    {
        $aAdminSessions = session()->all();

        $bValidateSession = $this->validateSessions($aAdminSessions);

        return [
            AdminConstants::ENTITY => $bValidateSession ? Arr::only($aAdminSessions, [
                AdminConstants::COLUMN_ADMIN_NO,
                AdminConstants::COLUMN_ADMIN_NAME,
                AdminConstants::COLUMN_ADMIN_USERNAME
            ]) : [],
            BaseConstants::HAS_SESSION => $bValidateSession
        ];
    }

    /**
     * This will destroy the admin session
     * @return array
     */
    public function destroyAdminSessions(): array
    {
        session()->flush();

        return [
            AdminConstants::ENTITY     => [],
            BaseConstants::HAS_SESSION => false
        ];
    }

    /**
     * This will validate the session if complete
     *
     * @param array $aAdminSessions
     * @return bool
     */
    private function validateSessions(array $aAdminSessions): bool
    {
        return Arr::has($aAdminSessions, [
            AdminConstants::COLUMN_ADMIN_NO,
            AdminConstants::COLUMN_ADMIN_NAME,
            AdminConstants::COLUMN_ADMIN_USERNAME
        ]);
    }
}
