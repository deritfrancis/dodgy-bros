<?php

namespace Tests\Unit\Controllers;

use App\Constants\BaseConstants;
use App\Http\Controllers\AuthController;
use App\Http\Requests\Auth\AuthRequest;
use App\Repositories\AdminRepository;
use App\Services\AuthService;
use Tests\Unit\Mock\Repositories\MockAdminRepository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

/**
 * @class AuthControllerTest
 * @package Tests\Unit
 * @author Francis Derit
 * @since 04.19.23
 */
class AuthControllerTest extends TestCase
{
    /**
     * @var AdminRepository|(AdminRepository&MockObject)|MockObject
     */
    private $oAdminRepositoryStub;

    public function setUp(): void
    {
        parent::setUp();
        $this->oAdminRepositoryStub = $this->createMock(AdminRepository::class);
        $this->oAuthController = new AuthController(new AuthService($this->oAdminRepositoryStub));
    }

    /**
     * @param array $aTestParameter
     * @return void
     * @dataProvider \Tests\Unit\DataProvider\Controllers\AuthControllerDataProvider::authenticateAdminTrueDataProvider();
     */
    public function test_method_authenticateAdmin_must_return_true(array $aTestParameter): void
    {
        $oRequest = new AuthRequest($aTestParameter[BaseConstants::PARAM]);
        $oRequest->validate($oRequest->rules());

        $this->oAdminRepositoryStub->method('getAdmin')->willReturn(MockAdminRepository::mockGetAdmin());
        $mActualResult = $this->oAuthController->authenticateAdmin($oRequest);

        $this->assertTrue($mActualResult);
    }

    /**
     * @param array $aTestParameter
     * @return void
     * @dataProvider \Tests\Unit\DataProvider\Controllers\AuthControllerDataProvider::authenticateAdminFalseDataProvider();
     */
    public function test_method_authenticateAdmin_must_return_false(array $aTestParameter): void
    {
        $oRequest = new AuthRequest($aTestParameter[BaseConstants::PARAM]);
        $oRequest->validate($oRequest->rules());

        $this->oAdminRepositoryStub->method('getAdmin')->willReturn(MockAdminRepository::mockGetAdminEmpty());
        $mActualResult = $this->oAuthController->authenticateAdmin($oRequest);

        $this->assertFalse($mActualResult);
    }
}
