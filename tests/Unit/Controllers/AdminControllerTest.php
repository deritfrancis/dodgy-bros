<?php

namespace Tests\Unit\Controllers;

use App\Http\Controllers\AdminController;
use App\Services\AdminService;
use Tests\TestCase;

/**
 * @class AdminControllerTest
 * @package Tests\Unit
 * @author Francis Derit
 * @since 04.19.23
 */
class AdminControllerTest extends TestCase
{
    /**
     * @var AdminService
     */
    private $oAdminService;

    public function setUp(): void
    {
        parent::setUp();
        $this->oAdminService = new AdminService();
        $this->oAdminController = new AdminController($this->oAdminService);
    }

    /**
     * @return void
     */
    public function test_method_getHome_must_return_view(): void
    {
        $this->oAdminController->getHome();

        $oRoute = $this->get('/');
        $oRoute->assertViewIs('app');
    }

    /**
     * @return void
     */
    public function test_method_logoutAdmin_must_return_false(): void
    {
        $this->oAdminController->logoutAdmin();

        $oRoute = $this->get('/');
        $oRoute->assertViewIs('app');
    }
}
