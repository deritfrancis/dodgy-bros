<?php

namespace Tests\Unit\Controllers;

use App\Constants\BaseConstants;
use App\Constants\CarsConstants;
use App\Http\Controllers\CarsController;
use App\Http\Requests\Cars\EditCarRequest;
use App\Http\Requests\Cars\GetCarsRequest;
use App\Http\Requests\Cars\SaveCarRequest;
use App\Repositories\CarsRepository;
use App\Repositories\SellersRepository;
use App\Services\CarsService;
use Tests\Unit\Mock\Repositories\MockSellersRepository;
use Tests\Unit\Mock\Repositories\MockCarsRepository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

/**
 * @class CarsControllerTest
 * @package Tests\Unit
 * @author Francis Derit
 * @since 04.19.23
 */
class CarsControllerTest extends TestCase
{
    /**
     * @var CarsRepository|(CarsRepository&MockObject)|MockObject
     */
    private $oCarsRepositoryStub;

    /**
     * @var SellersRepository|(SellersRepository&MockObject)|MockObject
     */
    private $oSellersRepositoryStub;

    public function setUp(): void
    {
        parent::setUp();
        $this->oCarsRepositoryStub = $this->createMock(CarsRepository::class);
        $this->oSellersRepositoryStub = $this->createMock(SellersRepository::class);

        $this->oCarsController = new CarsController(
            new CarsService($this->oCarsRepositoryStub, $this->oSellersRepositoryStub)
        );
    }

    /**
     * @param array $aTestParameter
     * @param array $mExpectedResult
     * @return void
     * @dataProvider \Tests\Unit\DataProvider\Controllers\CarsControllerDataProvider::getCarsAllDataProvider();
     */
    public function test_method_getCars_must_return_array_all_cars(array $aTestParameter, array $mExpectedResult): void
    {
        $oRequest = new GetCarsRequest($aTestParameter[BaseConstants::PARAM]);
        $oRequest->validate($oRequest->rules());

        $this->oCarsRepositoryStub->method('fetchCars')->willReturn(MockCarsRepository::mockFetchCars($aTestParameter[BaseConstants::PARAM]));
        $mActualResult = $this->oCarsController->getCars($oRequest);

        $this->assertEquals($mExpectedResult[BaseConstants::RESULT], $mActualResult);
    }

    /**
     * @param array $aTestParameter
     * @param array $mExpectedResult
     * @return void
     * @dataProvider \Tests\Unit\DataProvider\Controllers\CarsControllerDataProvider::getCarsFilterDataProvider();
     */
    public function test_method_getCars_must_return_array_cars_filter(array $aTestParameter, array $mExpectedResult): void
    {
        $oRequest = new GetCarsRequest($aTestParameter[BaseConstants::PARAM]);
        $oRequest->validate($oRequest->rules());

        $this->oCarsRepositoryStub->method('fetchCars')->willReturn(MockCarsRepository::mockFetchCars($aTestParameter[BaseConstants::PARAM]));
        $mActualResult = $this->oCarsController->getCars($oRequest);

        $this->assertEquals($mExpectedResult[BaseConstants::RESULT], $mActualResult);
    }


    /**
     * @param array $aTestParameter
     * @return void
     * @dataProvider \Tests\Unit\DataProvider\Controllers\CarsControllerDataProvider::saveCarSuccessDataProvider();
     */
    public function test_method_saveCar_must_return_true(array $aTestParameter): void
    {
        $oRequest = new SaveCarRequest($aTestParameter[BaseConstants::PARAM]);
        $oRequest->validate($oRequest->rules());

        $this->oSellersRepositoryStub->method('createSeller')->willReturn(MockSellersRepository::mockCreateSeller());
        $this->oCarsRepositoryStub->method('createCar')->willReturn(true);
        $mActualResult = $this->oCarsController->saveCar($oRequest);

        $this->assertTrue($mActualResult);
    }

    /**
     * @param array $aTestParameter
     * @return void
     * @dataProvider \Tests\Unit\DataProvider\Controllers\CarsControllerDataProvider::editCarSuccessDataProvider();
     */
    public function test_method_editCar_must_return_true(array $aTestParameter): void
    {
        $iCarNo = $aTestParameter[BaseConstants::PARAM][CarsConstants::COLUMN_CAR_NO];

        $oRequest = new EditCarRequest($aTestParameter[BaseConstants::PARAM]);
        $oRequest->validate($oRequest->rules());

        $this->oCarsRepositoryStub->method('findCar')->willReturn(MockCarsRepository::mockFindCar($iCarNo));
        $this->oCarsRepositoryStub->method('updateCars')->willReturn(true);
        $mActualResult = $this->oCarsController->editCar($oRequest, $iCarNo);

        $this->assertTrue($mActualResult);
    }

    /**
     * @param array $aTestParameter
     * @return void
     * @dataProvider \Tests\Unit\DataProvider\Controllers\CarsControllerDataProvider::editCarFailedDataProvider();
     */
    public function test_method_editCar_must_return_false(array $aTestParameter): void
    {
        $iCarNo = $aTestParameter[BaseConstants::PARAM][CarsConstants::COLUMN_CAR_NO];

        $oRequest = new EditCarRequest($aTestParameter[BaseConstants::PARAM]);
        $oRequest->validate($oRequest->rules());

        $this->oCarsRepositoryStub->method('findCar')->willReturn(MockCarsRepository::mockFindCar($iCarNo));
        $mActualResult = $this->oCarsController->editCar($oRequest, $iCarNo);

        $this->assertFalse($mActualResult);
    }

}
