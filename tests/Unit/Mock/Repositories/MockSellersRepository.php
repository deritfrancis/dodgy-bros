<?php

namespace Tests\Unit\Mock\Repositories;

use App\Constants\SellersConstants;

/**
 * @class MockSellersRepository
 * @package Tests\Unit\Mock\Repositories
 * @author Francis Derit
 * @since 04.19.23
 */
class MockSellersRepository
{
    public static function mockCreateSeller(): array
    {
        return [
            SellersConstants::COLUMN_SELLER_NO => 1,
            SellersConstants::COLUMN_SELLER_NAME => 'Francis Derit',
            SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
        ];
    }
}
