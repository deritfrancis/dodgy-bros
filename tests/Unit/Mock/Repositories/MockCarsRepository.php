<?php

namespace Tests\Unit\Mock\Repositories;

use App\Constants\CarsConstants;
use App\Constants\SellersConstants;
use Illuminate\Support\Arr;

/**
 * @class MockCarsRepository
 * @package Tests\Unit\Mock\Repositories
 * @author Francis Derit
 * @since 04.19.23
 */
class MockCarsRepository
{
    public static function mockFetchCars(array $aParams): array
    {
        if (Arr::hasAny($aParams, [CarsConstants::CATEGORY, CarsConstants::VALUE]) === true) {
            return [
                [
                    [
                        CarsConstants::COLUMN_CAR_NO => 1,
                        CarsConstants::COLUMN_SELLER_NO => 1,
                        CarsConstants::COLUMN_CAR_NAME => 'BWM',
                        CarsConstants::COLUMN_CAR_MODEL => 'M3',
                        CarsConstants::COLUMN_YEAR => 2022,
                        CarsConstants::COLUMN_CONDITION => 'excellent',
                        CarsConstants::COLUMN_PRICE => 85000,
                        CarsConstants::COLUMN_IS_AVAILABLE => 1,
                        SellersConstants::ENTITY => [
                            SellersConstants::COLUMN_SELLER_NO => 1,
                            SellersConstants::COLUMN_SELLER_NAME => 'Francis Derit',
                            SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                        ]
                    ]
                ],
                1
            ];
        }

        return [
            [
                [
                    CarsConstants::COLUMN_CAR_NO => 1,
                    CarsConstants::COLUMN_SELLER_NO => 1,
                    CarsConstants::COLUMN_CAR_NAME => 'BWM',
                    CarsConstants::COLUMN_CAR_MODEL => 'M3',
                    CarsConstants::COLUMN_YEAR => 2022,
                    CarsConstants::COLUMN_CONDITION => 'excellent',
                    CarsConstants::COLUMN_PRICE => 85000,
                    CarsConstants::COLUMN_IS_AVAILABLE => 1,
                    SellersConstants::ENTITY => [
                        SellersConstants::COLUMN_SELLER_NO => 1,
                        SellersConstants::COLUMN_SELLER_NAME => 'Francis Derit',
                        SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                    ]
                ],
                [
                    CarsConstants::COLUMN_CAR_NO => 2,
                    CarsConstants::COLUMN_SELLER_NO => 1,
                    CarsConstants::COLUMN_CAR_NAME => 'Jeep Wrangler',
                    CarsConstants::COLUMN_CAR_MODEL => 'Sport',
                    CarsConstants::COLUMN_YEAR => 2017,
                    CarsConstants::COLUMN_CONDITION => 'good',
                    CarsConstants::COLUMN_PRICE => 22000,
                    CarsConstants::COLUMN_IS_AVAILABLE => 1,
                    SellersConstants::ENTITY => [
                        SellersConstants::COLUMN_SELLER_NO => 1,
                        SellersConstants::COLUMN_SELLER_NAME => 'Francis Derit',
                        SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                    ]
                ],
                [
                    CarsConstants::COLUMN_CAR_NO => 3,
                    CarsConstants::COLUMN_SELLER_NO => 1,
                    CarsConstants::COLUMN_CAR_NAME => 'Ford F-150',
                    CarsConstants::COLUMN_CAR_MODEL => 'XLT',
                    CarsConstants::COLUMN_YEAR => 2008,
                    CarsConstants::COLUMN_CONDITION => 'poor',
                    CarsConstants::COLUMN_PRICE => 4000,
                    CarsConstants::COLUMN_IS_AVAILABLE => 1,
                    SellersConstants::ENTITY => [
                        SellersConstants::COLUMN_SELLER_NO => 1,
                        SellersConstants::COLUMN_SELLER_NAME => 'Francis Derit',
                        SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                    ]
                ]
            ],
            3
        ];
    }

    public static function mockFindCar(int $iCarNo): array
    {
        if ($iCarNo === 1) {
            return [
                [
                    [
                        CarsConstants::COLUMN_CAR_NO       => 1,
                        CarsConstants::COLUMN_SELLER_NO    => 1,
                        CarsConstants::COLUMN_CAR_NAME     => 'BWM',
                        CarsConstants::COLUMN_CAR_MODEL    => 'M3',
                        CarsConstants::COLUMN_YEAR         => 2022,
                        CarsConstants::COLUMN_CONDITION    => 'excellent',
                        CarsConstants::COLUMN_PRICE        => 85000,
                        CarsConstants::COLUMN_IS_AVAILABLE => 1,
                        SellersConstants::ENTITY => [
                            SellersConstants::COLUMN_SELLER_NO     => 1,
                            SellersConstants::COLUMN_SELLER_NAME   => 'Francis Derit',
                            SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                        ]
                    ],
                ],
            ];
        }

        return [];
    }
}
