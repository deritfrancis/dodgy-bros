<?php

namespace Tests\Unit\Mock\Repositories;

use App\Constants\AdminConstants;

/**
 * @class MockAdminRepository
 * @package Tests\Unit\Mock\Repositories
 * @author Francis Derit
 * @since 04.19.23
 */
class MockAdminRepository
{
    public static function mockGetAdmin(): array
    {
        return [
            [
                AdminConstants::COLUMN_ADMIN_NO       => 1,
                AdminConstants::COLUMN_ADMIN_NAME     => 'Iron Mike',
                AdminConstants::COLUMN_ADMIN_USERNAME => 'mike@example.org'
            ]
        ];
    }

    public static function mockGetAdminEmpty(): array
    {
        return [];
    }
}
