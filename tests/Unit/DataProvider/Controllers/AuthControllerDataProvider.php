<?php

namespace Tests\Unit\DataProvider\Controllers;

use App\Constants\AdminConstants;
use App\Constants\BaseConstants;

/**
 * @class AuthControllerDataProvider
 * @package Tests\Unit\DataProvider\Controllers
 * @author Francis Derit
 * @since 04.19.23
 */
class AuthControllerDataProvider
{
    public static function authenticateAdminTrueDataProvider(): array
    {
        return [
            [
                [
                    BaseConstants::PARAM => [
                        AdminConstants::COLUMN_ADMIN_USERNAME => 'mike@example.org',
                        AdminConstants::COLUMN_ADMIN_PASSWORD => 'mikeymike123'
                    ],
                ]
            ]
        ];
    }

    public static function authenticateAdminFalseDataProvider(): array
    {
        return [
            [
                [
                    BaseConstants::PARAM => [
                        AdminConstants::COLUMN_ADMIN_USERNAME => 'failed_username@example.org',
                        AdminConstants::COLUMN_ADMIN_PASSWORD => 'failed_password'
                    ],
                ]
            ]
        ];
    }
}
