<?php

namespace Tests\Unit\DataProvider\Controllers;

use App\Constants\BaseConstants;
use App\Constants\CarsConstants;
use App\Constants\SellersConstants;

/**
 * @class CarsControllerDataProvider
 * @package Tests\Unit\DataProvider\Controllers
 * @author Francis Derit
 * @since 04.19.23
 */
class CarsControllerDataProvider
{
    public static function getCarsAllDataProvider(): array
    {
        return [
            [
                [
                    BaseConstants::PARAM => [
                        BaseConstants::OFFSET => 0
                    ]
                ],
                [
                    BaseConstants::RESULT => [
                        CarsConstants::ENTITY => [
                            [
                                CarsConstants::COLUMN_CAR_NO       => 1,
                                CarsConstants::COLUMN_SELLER_NO    => 1,
                                CarsConstants::COLUMN_CAR_NAME     => 'BWM',
                                CarsConstants::COLUMN_CAR_MODEL    => 'M3',
                                CarsConstants::COLUMN_YEAR         => 2022,
                                CarsConstants::COLUMN_CONDITION    => 'excellent',
                                CarsConstants::COLUMN_PRICE        => 85000,
                                CarsConstants::COLUMN_IS_AVAILABLE => 1,
                                SellersConstants::ENTITY => [
                                    SellersConstants::COLUMN_SELLER_NO     => 1,
                                    SellersConstants::COLUMN_SELLER_NAME   => 'Francis Derit',
                                    SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                                ]
                            ],
                            [
                                CarsConstants::COLUMN_CAR_NO       => 2,
                                CarsConstants::COLUMN_SELLER_NO    => 1,
                                CarsConstants::COLUMN_CAR_NAME     => 'Jeep Wrangler',
                                CarsConstants::COLUMN_CAR_MODEL    => 'Sport',
                                CarsConstants::COLUMN_YEAR         => 2017,
                                CarsConstants::COLUMN_CONDITION    => 'good',
                                CarsConstants::COLUMN_PRICE        => 22000,
                                CarsConstants::COLUMN_IS_AVAILABLE => 1,
                                SellersConstants::ENTITY => [
                                    SellersConstants::COLUMN_SELLER_NO     => 1,
                                    SellersConstants::COLUMN_SELLER_NAME   => 'Francis Derit',
                                    SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                                ]
                            ],
                            [
                                CarsConstants::COLUMN_CAR_NO       => 3,
                                CarsConstants::COLUMN_SELLER_NO    => 1,
                                CarsConstants::COLUMN_CAR_NAME     => 'Ford F-150',
                                CarsConstants::COLUMN_CAR_MODEL    => 'XLT',
                                CarsConstants::COLUMN_YEAR         => 2008,
                                CarsConstants::COLUMN_CONDITION    => 'poor',
                                CarsConstants::COLUMN_PRICE        => 4000,
                                CarsConstants::COLUMN_IS_AVAILABLE => 1,
                                SellersConstants::ENTITY => [
                                    SellersConstants::COLUMN_SELLER_NO     => 1,
                                    SellersConstants::COLUMN_SELLER_NAME   => 'Francis Derit',
                                    SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                                ]
                            ]
                        ],
                        BaseConstants::COUNT => 3
                    ]
                ]
            ]
        ];
    }

    public static function getCarsFilterDataProvider(): array
    {
        return [
            [
                [
                    BaseConstants::PARAM => [
                        BaseConstants::OFFSET => 0,
                        CarsConstants::CATEGORY => CarsConstants::COLUMN_CAR_NAME,
                        CarsConstants::VALUE => 'BMW'
                    ]
                ],
                [
                    BaseConstants::RESULT => [
                        CarsConstants::ENTITY => [
                            [
                                CarsConstants::COLUMN_CAR_NO       => 1,
                                CarsConstants::COLUMN_SELLER_NO    => 1,
                                CarsConstants::COLUMN_CAR_NAME     => 'BWM',
                                CarsConstants::COLUMN_CAR_MODEL    => 'M3',
                                CarsConstants::COLUMN_YEAR         => 2022,
                                CarsConstants::COLUMN_CONDITION    => 'excellent',
                                CarsConstants::COLUMN_PRICE        => 85000,
                                CarsConstants::COLUMN_IS_AVAILABLE => 1,
                                SellersConstants::ENTITY => [
                                    SellersConstants::COLUMN_SELLER_NO     => 1,
                                    SellersConstants::COLUMN_SELLER_NAME   => 'Francis Derit',
                                    SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                                ]
                            ],
                        ],
                        BaseConstants::COUNT => 1
                    ]
                ]
            ]
        ];
    }

    public static function saveCarSuccessDataProvider(): array
    {
        return [
            [
                [
                    BaseConstants::PARAM => [
                        CarsConstants::COLUMN_CAR_NAME => 'Ford Mustang',
                        CarsConstants::COLUMN_CAR_MODEL => 'GT Coupe',
                        CarsConstants::COLUMN_YEAR => 1967,
                        CarsConstants::COLUMN_CONDITION => 'excellent',
                        CarsConstants::COLUMN_PRICE => 50000,
                        SellersConstants::COLUMN_SELLER_NAME => 'Francis Derit',
                        SellersConstants::COLUMN_SELLER_NUMBER => '09157476395',
                    ]
                ]
            ]
        ];
    }

    public static function editCarSuccessDataProvider(): array
    {
        return [
            [
                [
                    BaseConstants::PARAM => [
                        CarsConstants::COLUMN_CAR_NO => 1,
                        CarsConstants::BUYER_NAME => null,
                        CarsConstants::BUYER_NUMBER => null,
                    ]
                ]
            ]
        ];
    }

    public static function editCarFailedDataProvider(): array
    {
        return [
            [
                [
                    BaseConstants::PARAM => [
                        CarsConstants::COLUMN_CAR_NO => 2,
                        CarsConstants::BUYER_NAME => null,
                        CarsConstants::BUYER_NUMBER => null,
                    ]
                ]
            ]
        ];
    }
}
