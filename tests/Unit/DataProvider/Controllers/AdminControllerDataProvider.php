<?php

namespace Tests\Unit\DataProvider\Controllers;

use App\Constants\AdminConstants;
use App\Constants\BaseConstants;

/**
 * @class AdminControllerDataProvider
 * @package Tests\Unit\DataProvider\Controllers
 * @author Francis Derit
 * @since 04.19.23
 */
class AdminControllerDataProvider
{
    public static function getHomeSuccessDataProvider(): array
    {
        return [
            [
                [
                    BaseConstants::PARAM => [
                        AdminConstants::COLUMN_ADMIN_NO       => 1,
                        AdminConstants::COLUMN_ADMIN_NAME     => 'Iron Mike',
                        AdminConstants::COLUMN_ADMIN_USERNAME => 'mike@example.org',
                    ],
                ]
            ]
        ];
    }
}
